package main

import (
    "fmt"
    "os"
    "flag"
    "gim/image"
    "image"
)

type RGBAEntry struct{
  red uint8
  green uint8
  blue uint8
  alpha uint8
}

 func main(){

  imagePtr := flag.String("image", "None", "Image to use")
  // outLocationPtr := flag.String("out", "out.jpg", "Output file location")
  // featureThreadhPtr := flag.Int("thresh", 10, "Output file location")

  flag.Parse()

  if *imagePtr == "None" {
   flag.PrintDefaults()
   os.Exit(1)
  }

  fmt.Println(*imagePtr)
  img := hue.ReadImage(*imagePtr)
  // features(*featureThreadhPtr, *outLocationPtr, img)
  grayScale(img)
}

func grayScale(img image.Image){
  imgInt := hue.GetUint8RGBA(img)

  imgLightness := imgInt.Lightness()
  imgAverage := imgInt.Average()
  imgLuminosity := imgInt.Luminosity()
  hue.WriteImage(&imgLightness, "lightness.jpg")
  hue.WriteImage(&imgAverage, "average.jpg")
  hue.WriteImage(&imgLuminosity, "luminosity.jpg")


}


// func features(featureThreadhPtr int, outLocationPtr string, img image.Image){
//   imgInt := hue.GetUint8RGBA(img)
//   a := imgInt.Features(uint8(featureThreadhPtr))
// }