package hue

import (
    "image/color"
    "image/png"
    _ "image/jpeg"
    "os"
    "fmt"
    "image"
    "math"
)

// image intermediary
type RGBAEntry struct{
  Red uint8
  Green uint8
  Blue uint8
  Alpha uint8
}

type RGBAPic struct {
  pic [][]RGBAEntry
  xMin int
  xMax int
  yMin int
  yMax int
}

func (pic *RGBAEntry) getColors() (uint8,uint8,uint8, uint8){
  return pic.Red, pic.Blue, pic.Green, pic.Alpha
}

func (pic *RGBAEntry) setColors(Red uint8, Blue uint8, Green uint8, Alpha uint8){
  pic.Red = Red
  pic.Blue = Blue
  pic.Green = Green
  pic.Alpha = Alpha
}

func (pic *RGBAPic) setBounds(xMin int, xMax int, yMin int, yMax int){
  pic.xMin = xMin
  pic.xMax = xMax
  pic.yMin = yMin
  pic.yMax = yMax
}


func ReadImage(filename string) image.Image{
  reader, err := os.Open(filename)
  if err != nil {
    fmt.Println(err)
    os.Exit(1)
  }

  m, _ , err := image.Decode(reader)
  if err != nil {
    fmt.Println(err)
    os.Exit(1)
  }
  return m
}

func makeRGBA(xMin int, xMax int, yMin int, yMax int) RGBAPic {
  var customRGBA RGBAPic
  customRGBA.pic = make([][]RGBAEntry, xMax)
  for i := range customRGBA.pic {
    customRGBA.pic[i] = make([]RGBAEntry, yMax)
  }
  customRGBA.setBounds(xMin, xMax, yMin, yMax)
  return customRGBA
}

func GetUint8RGBA(pic image.Image) RGBAPic {
  bounds := pic.Bounds()
  // myImage := image.NewRGBA(image.Rect(bounds.Min.X,bounds.Min.Y,bounds.Max.X,bounds.Max.Y))

  var customRGBA RGBAPic
  customRGBA.pic = make([][]RGBAEntry, bounds.Max.X)
  for i := range customRGBA.pic{
  customRGBA.pic[i] = make([]RGBAEntry, bounds.Max.Y)
  }

  for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
    for x := bounds.Min.X; x < bounds.Max.X; x++ {
      r, g, b, a := pic.At(x, y).RGBA()
      customRGBA.pic[x][y].Red = uint8(r/257)
      customRGBA.pic[x][y].Blue = uint8(g/257)
      customRGBA.pic[x][y].Green = uint8(b/257)
      customRGBA.pic[x][y].Alpha = uint8(a/257)
    }
  }

  customRGBA.xMin = bounds.Min.X
  customRGBA.yMin = bounds.Min.Y
  customRGBA.xMax = bounds.Max.X
  customRGBA.yMax = bounds.Max.Y

  return customRGBA
}

func GetAverageColor(pic *RGBAPic) RGBAEntry {
  var Red, Green, Blue = 0, 0, 0
  for y := pic.yMin; y < pic.yMax; y++ {
    for x := pic.xMin; x < pic.xMax; x++ {
      Red = int(pic.pic[x][y].Red) + Red
      Blue = int(pic.pic[x][y].Blue) + Blue
      Green = int(pic.pic[x][y].Green) + Green
    }
  }
  Red = Red/((pic.yMax*pic.xMax))
  Blue = Blue/((pic.yMax*pic.xMax))
  Green = Green/((pic.yMax*pic.xMax))

  var pixel RGBAEntry
  pixel.Red = uint8(Red)
  pixel.Blue = uint8(Blue)
  pixel.Green = uint8(Green)


  return pixel
}

func (pic *RGBAPic) Lightness() RGBAPic {
  out := makeRGBA(pic.xMin, pic.xMax, pic.yMin, pic.yMax)
  for y := pic.yMin; y < pic.yMax; y++ {
    for x := pic.xMin; x < pic.xMax; x++ {
      r, b, g, a := pic.pic[x][y].getColors()
      light := (math.Max(math.Max(float64(r),float64(g)),float64(b)) + math.Min(math.Min(float64(r),float64(g)),float64(b)))/2
      out.pic[x][y].setColors(uint8(light), uint8(light), uint8(light), a)
    }
  }
  return out
}

func (pic *RGBAPic) Average() RGBAPic {
  out := makeRGBA(pic.xMin, pic.xMax, pic.yMin, pic.yMax)
  for y := pic.yMin; y < pic.yMax; y++ {
    for x := pic.xMin; x < pic.xMax; x++ {
      r, b, g, a := pic.pic[x][y].getColors()
      average := (r+g+b)/3
      out.pic[x][y].setColors(average, average, average, a)
    }
  }
  return out
}


func (pic *RGBAPic)  Luminosity() RGBAPic {
  out := makeRGBA(pic.xMin, pic.xMax, pic.yMin, pic.yMax)
  for y := pic.yMin; y < pic.yMax; y++ {
    for x := pic.xMin; x < pic.xMax; x++ {
      r, b, g, a := pic.pic[x][y].getColors()
      lumen := uint8(0.21*float64(r)) + uint8(.72*float64(g)) + uint8(.07*float64(b))
      out.pic[x][y].setColors(lumen, lumen, lumen, a)
    }
  }
  return out
}

func WriteImage(pic *RGBAPic, saveLocation string) {
  myImage := image.NewRGBA(image.Rect(pic.xMin, pic.yMin, pic.xMax, pic.yMax))
  for y := pic.yMin; y < pic.yMax; y++ {
    for x := pic.xMin; x < pic.xMax; x++ {
      myImage.Set(x,y,color.RGBA{
        pic.pic[x][y].Red,
        pic.pic[x][y].Blue,
        pic.pic[x][y].Green,
        pic.pic[x][y].Alpha,
      })
    }
  }

  outputFile, err := os.Create(saveLocation)
  if err != nil {
    fmt.Println(err)
    os.Exit(1)
  }
  png.Encode(outputFile, myImage)
  outputFile.Close()
}


func (pic *RGBAPic) Features(threshhold uint8)  RGBAPic {
  common := GetAverageColor(pic)
  out := makeRGBA(pic.xMin, pic.xMax, pic.yMin, pic.yMax)
   for y := pic.yMin; y < pic.yMax; y++ {
    for x := pic.xMin; x < pic.xMax; x++ {
      if !between(pic.pic[x][y], threshhold, common) {
        out.pic[x][y].setColors(255,255,255,255)
      } else {
        out.pic[x][y].setColors(0,0,0,255)
    }
    }
  }
  return out
}


func between(entry RGBAEntry, bound uint8, compareValue RGBAEntry) bool {
    if math.Abs(float64(entry.Red) - float64(compareValue.Red)) > float64(bound){
      return false
    }
    if math.Abs(float64(entry.Blue) - float64(compareValue.Blue)) > float64(bound){
      return false
    }
    if math.Abs(float64(entry.Green) - float64(compareValue.Green)) > float64(bound){
      return false
    }

    return true
}